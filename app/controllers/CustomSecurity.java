package controllers;

import models.User;
import play.cache.Cache;

public class CustomSecurity extends Secure.Security {

    public static Void checkValidSession(){

        Object cache = Cache.get(session.get("id"));
        if (cache == null || !cache.toString().equals(session.getAuthenticityToken())){
            redirect("/login");
        }

        return null;
    }

    public static Void validateSession(){

        Cache.add(session.get("id"), session.getAuthenticityToken(), "30mn");
        return null;
    }

    static void onDisconnect(){

        Cache.delete(session.get("id"));
    }

    static boolean authenticate(String username, String password) {

        User user = User.getUserFromLogin(username, password);
        if (user != null) {
            session.put("id", user.getId());
            session.put("user", user.getUsername());

            validateSession();

            return true;

        } else {
            return false;
        }

    }

    static boolean check(String profile) {

        User user = User.getById( Integer.parseInt(session.get("id")) );
        if(user != null && "admin".equals(profile)){
            return user.isAdmin();
        }

        return false;
    }

}

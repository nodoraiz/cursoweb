package utils;

import play.Play;

public class Constants {

    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://localhost/web";
    public static final String USER = Play.configuration.getProperty("db.mysql.user");
    public static final String PASSWORD = Play.configuration.getProperty("db.mysql.pass");
}


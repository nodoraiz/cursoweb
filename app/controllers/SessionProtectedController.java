package controllers;

import play.mvc.Before;
import play.mvc.Controller;

public class SessionProtectedController extends Controller {

    @Before
    public static void before(){
        CustomSecurity.checkValidSession();
    }

}

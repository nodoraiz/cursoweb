package controllers;

import models.User;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.With;
import utils.GenericFunctions;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@With(Secure.class)
public class Profile extends SessionProtectedController {

    public static void edit(int id, String password1, String password2, String address){

        if(session.get("id") == null || !session.get("id").equals(""+id)){
            unauthorized();
        }

        if(request.method.equals("POST")) {
            checkAuthenticity();
        }

        User user = User.getById(id);

        boolean save = true;
        if(password1 != null && password2 != null && !password1.equals("")){

            if(password1.equals(password2)){
                try {
                    user.setPassword(GenericFunctions.generateMD5(password1));

                } catch (Exception e){
                    e.printStackTrace();
                    save = false;
                }

            } else {
                String url = "/profile/edit?id=" + id;
                Index.error("Password doesn't match", url, Crypto.sign(url));
                save = false;
            }

        }

        if(save && address != null){
            user.setAddress(address);
            user.save();
            flash.put("info", "Changes saved");
        }

        // edit the properties of the user
        render(user);
    }

    @Check("admin")
    public static void listUsers(){

        List<User> userList = User.getAll();
        render(userList);
    }

    @Check("admin")
    public static void addUser(String username, String password, String address, boolean isAdmin){

        if(username == null || password == null || address == null
                || username.isEmpty() || password.isEmpty() || address.isEmpty()){
            String url = "/profile/users";
            Index.error("All fields have to be defined", url, Crypto.sign(url));
        }

        new User(username, password, isAdmin, address).save();
        listUsers();
    }
}

package controllers;

import models.Message;
import models.User;
import play.libs.Crypto;
import play.mvc.Controller;
import play.mvc.With;

import java.util.ArrayList;
import java.util.List;

@With(Secure.class)
public class Index extends SessionProtectedController {

    public static void index(){

        int id = Integer.parseInt( session.get("id") );

        List<Message> sentMessageList = new ArrayList<Message>();
        List<Message> receivedMessageList = new ArrayList<Message>();
        for(Message message : Message.getByUser(id)){
            if(message.getIdUserFrom() == id){
                sentMessageList.add(message);
            } else {
                receivedMessageList.add(message);
            }
        }
        
        User user = User.getById(id);
        List<User> userList = new ArrayList<User>();
        for(User iuser : User.getAll()){
            if(iuser.getId() != id){
                userList.add(iuser);
            }
        }

        render(user, sentMessageList, receivedMessageList, userList);
    }

    public static void list(){

        List<Message> messageList = Message.getAll();
        render(messageList);
    }

    public static void addMessage(int userIdFrom, int userIdTo, String message){

        if(message == null || message.isEmpty()){
            String url = "/";
            error("You have to write a message", url, Crypto.sign(url));
        }

        new Message(userIdFrom, userIdTo, message).save();
        index();
    }

    public static void error(String message, String back, String sign){

        if(sign == null || !sign.equals(Crypto.sign(back))){
            back = "/";
        }

        flash.put("error", message);
        render(back);
    }

}

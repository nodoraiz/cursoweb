package models;

import utils.Constants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Message {

    private int id;
    private int idUserFrom;
    private int idUserTo;
    private String message;
    private String usernameFrom;
    private String usernameTo;

    public int getId() {
        return id;
    }

    public int getIdUserFrom() {
        return idUserFrom;
    }

    public int getIdUserTo() {
        return idUserTo;
    }

    public String getUsernameFrom() {
        return usernameFrom;
    }

    public String getUsernameTo() {
        return usernameTo;
    }

    public String getMessage() {
        return message;
    }

    public Message(int idUserFrom, int idUserTo, String message) {
        this.idUserFrom = idUserFrom;
        this.idUserTo = idUserTo;
        this.message = message;
    }

    public Message(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.idUserFrom = resultSet.getInt("idUserFrom");
        this.idUserTo = resultSet.getInt("idUserTo");
        this.message = resultSet.getString("message");
    }

    public static List<Message> getAll(){

        List<Message> messageList = new ArrayList<Message>();

        try {
            Connection connection = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT m.id, m.idUserFrom, m.idUserTo, m.message, u1.username as usernameFrom, u2.username as usernameTo " +
                            "FROM messages m " +
                            "INNER JOIN users u1 ON u1.id = m.idUserFrom " +
                            "INNER JOIN users u2 ON u2.id = m.idUserTo " +
                            "ORDER BY id DESC;"
            );

            ResultSet resultSet = preparedStatement.executeQuery();
            Message message;
            while(resultSet.next()){
                message = new Message(resultSet);
                message.usernameFrom = resultSet.getString("usernameFrom");
                message.usernameTo = resultSet.getString("usernameTo");
                messageList.add(message);
            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch (Exception e){
            e.printStackTrace();
        }

        return messageList;
    }

    public static List<Message> getByUser(int idUser){

        List<Message> messageList = new ArrayList<Message>();

        try {
            Connection connection = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement statement = connection.createStatement();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT m.id, m.idUserFrom, m.idUserTo, m.message, u1.username as usernameFrom, u2.username as usernameTo " +
                            "FROM messages m " +
                            "INNER JOIN users u1 ON u1.id = m.idUserFrom " +
                            "INNER JOIN users u2 ON u2.id = m.idUserTo " +
                            "WHERE u1.id=? OR u2.id=? " +
                            "ORDER BY id DESC;"
            );
            preparedStatement.setInt(1, idUser);
            preparedStatement.setInt(2, idUser);

            ResultSet resultSet = preparedStatement.executeQuery();
            Message message;
            while(resultSet.next()){
                message = new Message(resultSet);
                message.usernameFrom = resultSet.getString("usernameFrom");
                message.usernameTo = resultSet.getString("usernameTo");
                messageList.add(message);
            }

            resultSet.close();
            statement.close();
            connection.close();

        } catch (Exception e){
            e.printStackTrace();
        }

        return messageList;
    }

//    public static Message getById(int id){
//
//        Message message = null;
//
//        try {
//            Connection conn = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
//            Statement stmt = conn.createStatement();
//            String sql = "SELECT id, idUser, message FROM messages WHERE id=?;";
//            PreparedStatement prest = conn.prepareStatement(sql);
//            prest.setInt(1, id);
//
//            ResultSet resultSet = prest.executeQuery();
//
//            while(resultSet.next()){
//                message = new Message(resultSet);
//            }
//
//            resultSet.close();
//            stmt.close();
//            conn.close();
//
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//
//        return message;
//    }

    public void save(){

        try {
            Connection connection = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement statement = connection.createStatement();

            PreparedStatement preparedStatement;
            if(this.id == 0){
                preparedStatement = connection.prepareStatement("INSERT INTO messages (idUserFrom, idUserTo, message) VALUES (?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setInt(1, this.idUserFrom);
                preparedStatement.setInt(2, this.idUserTo);
                preparedStatement.setString(3, this.message);

            } else {
                preparedStatement = connection.prepareStatement("UPDATE messages SET message=? WHERE id=?;");
                preparedStatement.setString(1, this.message);
                preparedStatement.setInt(2, this.id);
            }

            this.id = preparedStatement.executeUpdate();

            statement.close();
            connection.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}

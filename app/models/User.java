package models;

import org.eclipse.jdt.internal.eval.CodeSnippetQualifiedNameReference;
import utils.Constants;
import utils.GenericFunctions;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String username;
    private String password;
    private boolean isAdmin;
    private String address;

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public String getAddress() {
        return address;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User(String username, String password, boolean isAdmin, String address) {
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;
        this.address = address;
    }

    public User(ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.username = resultSet.getString("username");
        this.password = resultSet.getString("password");
        this.address = resultSet.getString("address");
        this.isAdmin = resultSet.getBoolean("isAdmin");
    }

    public static User getUserFromLogin(String username, String password){

        User user = null;

        try {
            password = GenericFunctions.generateMD5(password);

            Connection conn = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement stmt = conn.createStatement();
            String sql = "SELECT id, username, password, isAdmin, address FROM users WHERE " +
                    "username=? AND password=?;";

            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);

            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()){
                user = new User(resultSet);
            }

            resultSet.close();
            stmt.close();
            conn.close();

        } catch (Exception e){
            e.printStackTrace();
        }

        return user;
    }

    public static List<User> getAll(){

        List<User> userList = new ArrayList<User>();

        try {
            Connection conn = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement stmt = conn.createStatement();
            String sql = "SELECT id, username, password, isAdmin, address FROM users ORDER BY id DESC;";
            PreparedStatement prest = conn.prepareStatement(sql);

            ResultSet resultSet = prest.executeQuery();

            while(resultSet.next()){
                userList.add( new User(resultSet) );
            }

            resultSet.close();
            stmt.close();
            conn.close();

        } catch (Exception e){
            e.printStackTrace();
        }

        return userList;
    }

    public static User getById(int id){

        User user = null;

        try {
            Connection conn = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement stmt = conn.createStatement();
            String sql = "SELECT id, username, password, isAdmin, address FROM users WHERE id=?;";
            PreparedStatement prest = conn.prepareStatement(sql);
            prest.setInt(1, id);

            ResultSet resultSet = prest.executeQuery();

            while(resultSet.next()){
                user = new User(resultSet);
            }

            resultSet.close();
            stmt.close();
            conn.close();

        } catch (Exception e){
            e.printStackTrace();
        }

        return user;
    }

    public void save(){

        try {
            Connection connection = DriverManager.getConnection(Constants.DB_URL, Constants.USER, Constants.PASSWORD);
            Statement statement = connection.createStatement();

            PreparedStatement preparedStatement;
            if(this.id == 0){
                preparedStatement = connection.prepareStatement("INSERT INTO users (username, password, address, isAdmin) VALUES (?, ?, ?, ?);", PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1, this.username);
                preparedStatement.setString(2, this.password);
                preparedStatement.setString(3, this.address);
                preparedStatement.setBoolean(4, this.isAdmin);

            } else {
                preparedStatement = connection.prepareStatement("UPDATE users SET username=?, password=?, address=?, isAdmin=? WHERE id=?;");
                preparedStatement.setString(1, this.username);
                preparedStatement.setString(2, this.password);
                preparedStatement.setString(3, this.address);
                preparedStatement.setBoolean(4, this.isAdmin);
                preparedStatement.setInt(5, this.id);
            }

            this.id = preparedStatement.executeUpdate();

            statement.close();
            connection.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
